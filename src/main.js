import Vue from "vue";
import Vuesax from "vuesax";
import "material-icons/iconfont/material-icons.css";
import "vuesax/dist/vuesax.css";
import router from "./router";
import App from "./App.vue";
import "./registerServiceWorker";

Vue.config.productionTip = false;

Vue.use(Vuesax);
new Vue({
    router,
    render: (h) => h(App),
}).$mount("#app");